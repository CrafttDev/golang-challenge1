package drum

import (
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"os"
)

var Token = [6]byte{'S', 'P', 'L', 'I', 'C', 'E'}

// DecodeFile decodes the drum machine file found at the provided path
// and returns a pointer to a parsed pattern which is the entry point to the
// rest of the data.
// TODO: implement
func DecodeFile(path string) (*Pattern, error) {
	p := &Pattern{}

	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	err = binary.Read(f, binary.LittleEndian, &p.Header)
	if p.Header.Token != Token {
		return nil, errors.New("Invalid valid file Token")
	}

	fmt.Printf("DecodeFile :: %s ::: Lets do this, Dammit\n", path)
	for err == nil {
		t := Track{}
		err = decodeTrack(f, &t)
		if err != nil {
			if err == io.EOF {
				return p, nil
			}
			fmt.Printf("decodeTrack Error :: %s", err.Error())
			return nil, err
		}
		fmt.Printf("DecodeFile :: append track :::  %s\n", string(t.Name[:t.NameLen]))
		p.Tracks = append(p.Tracks, t)
	}

	return p, nil
}

// readTrack reads an individual track.
// If the index is over 255, we have hit corrupt data and just return EOF to allow any previous tracks to be considered valid.
func decodeTrack(r io.Reader, t *Track) error {
	err := binary.Read(r, binary.LittleEndian, &t.Index)
	if err != nil {
		return err
	}
	if t.Index > 255 {
		return io.EOF
	}
	err = binary.Read(r, binary.LittleEndian, &t.NameLen)
	if err != nil {
		return err
	}
	t.Name = make([]byte, t.NameLen)
	err = binary.Read(r, binary.LittleEndian, &t.Name)
	if err != nil {
		return err
	}
	err = binary.Read(r, binary.LittleEndian, &t.QuarterNotes)
	return err
}

// Pattern is the high level representation of the
// drum pattern contained in a .splice file.
// TODO: implement
type Pattern struct {
	Header Header
	Tracks []Track
}

type Header struct {
	Token   [6]byte
	_       [8]byte
	Version [18]byte
	_       [14]byte
	Tempo   float32
}

type Track struct {
	Index        int32
	NameLen      byte
	Name         []byte
	QuarterNotes [16]byte
}
