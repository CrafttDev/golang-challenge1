// Package drum is supposed to implement the decoding of .splice drum machine files.
// See golang-challenge.com/go-challenge1/ for more information
package drum

import (
	"fmt"
	"path/filepath"
)

func DrumFile(file string) {
	//I would not use this file unless I expanded example to actually play the drum or something etc, so this is just to give it some type of purpose
	if filepath.Ext(file) != ".splice" {
		fmt.Println("Error! file must be vm file")
		return
	}

	DrumPattern, err := DecodeFile(file)
	if err != nil {
		fmt.Printf("DrumFile :: %s is not a valid splice file \n", file)
		return
	}

	fmt.Printf("Getting ready to play version: %s", DrumPattern.Header.Version)
	fmt.Printf(" at Tempo: %v \n", DrumPattern.Header.Tempo)
	fmt.Printf(" at tracks: %v \n", len(DrumPattern.Tracks))

	for _, t := range DrumPattern.Tracks {
		fmt.Printf(" Play Track: %s \n", string(t.Name[:t.NameLen]))
	}

	fmt.Println("Everything is Wonderful")
}
