package main

import (
	"main/golang-challenge1/drum"
	"os"
	"path/filepath"
)

func main() {
	var files []string

	root := "../drum/fixtures"
	err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		if info.IsDir() {
			return nil
		}
		if filepath.Ext(path) == ".splice" {
			files = append(files, path)
		}
		return nil
	})

	if err != nil {
		panic(err)
	}

	for _, file := range files {
		drum.DrumFile(file)
	}
}
